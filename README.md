Dans le cadre de la 4e échéance de la Directive Bruit, plusieurs bases de données ont été consolidées par le Cerema en vue de la réalisation des cartes de bruit stratégiques par NoiseModelling. 

Ce projet gitlab se consacre à la recherche des niveaux de bruits auxquels sont exposés les établissements recevant du public sensible.
